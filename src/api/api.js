import axios from "axios";

const instance = axios.create({
    withCredentials:true,
    baseURL: 'https://social-network.samuraijs.com/api/1.0/',
    headers: {
        "API-KEY": "c7e9ba30-39b3-4bf7-aed9-4b94a214c55a"
    }

})

export const usersAPI = {
    getUsers(currentPage = 1,pageSize = 10){
        return  instance.get(`users?page=${currentPage}&count=${pageSize}`)
            .then(response => {
                return response.data;
            })
    },
    getProfile(id = 2){
        return instance.get(`profile/` + id)
            .then(response => {
                return response.data;
            });
    },
    deleteUser(id){
        return instance.delete(`follow/${id}`)
    },
    addUser(id){
        return instance.post(`follow/${id}`)
    }
}


