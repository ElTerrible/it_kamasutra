import profileReducer from "./profile-reducer";
import dialogsReducer from "./dialogs-reducer";
import sidebarReducer from "./sidebar-reducer";


let store = {
    _state:{
        profilePage: {
            posts: [
                {id: 1, message: 'Hi, how are you?', likesCount: 12},
                {id: 2, message: 'It\'s my first post', likesCount: 34},
                {id: 3, message: 'Hello world!', likesCount: 34},
            ],
            newPostText: 'Breanne Benson',
        },
        dialogsPage: {
            dialogs: [
                {id: 1, name: 'Morales'},
                {id: 2, name: 'Marques'},
                {id: 3, name: 'Barrera'},
                {id: 4, name: 'Pacman'},
                {id: 5, name: 'Tyson'},
            ],
            messages: [
                {id:1, message: 'Hi'},
                {id:2, message: 'Hi, how are you?'},
                {id:1, message: 'Yo Yo'},
            ],
            newMessageBody: '',
        },
        sidebar:{},
    },
    _callsubcriber() {
        console.log('state is changed');
    },
    getState(){
        return this._state;
    },
    subscribe(observer){
        this._callsubcriber = observer;
    },
    addPost(){

    },
    updateNewPostText(newText){

    },
    dispatch(action){
        this._state.profilePage = profileReducer(this._state.profilePage, action);  // { type: 'UPDATE-NEW-POST-TEXT', newText: 'qwerty'}
        this._state.dialogsPage = dialogsReducer(this._state.dialogsPage, action);
        this._state.sidebar = sidebarReducer(this._state.sidebar, action);
        this._callsubcriber(this._state);
    },
}


export default store;
window.store = store;