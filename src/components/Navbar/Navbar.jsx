import s from './Navbar.module.css'
import {NavLink} from "react-router-dom";

const Navbar = () => {
    return (

        <nav className={s.nav}>
            <ul>
                <li><NavLink className = { navData => navData.isActive ? s.active : s.link} to='/profile'>Profile</NavLink></li>
                <li><NavLink className={ navData => navData.isActive ? s.active : s.link} to='/dialogs'>Messages</NavLink></li>
                <li><NavLink className={ navData => navData.isActive ? s.active : s.link} to='/users'>Users</NavLink></li>
                <li><NavLink className={ navData => navData.isActive ? s.active : s.link} to='/news'>News</NavLink></li>
                <li><NavLink className={ navData => navData.isActive ? s.active : s.link}  to='/music'>Music</NavLink></li>
                <li><a className={s.link} href='#'>Settings</a></li>
            </ul>
        </nav>
    )
}


export default Navbar;