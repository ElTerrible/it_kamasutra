import s from './Post.module.css';

const Post = (props) => {
    return (
        <div className={s.item}>
            <img src="https://thumb-p2.xhcdn.com/a/XPS9RA5wvzGlbTHem4_Q_A/000/406/011/042_1000.jpg" alt=""/>
            {props.message}
            <div>
                <span>like {props.likesCount}</span>
            </div>
        </div>
        )
}


export default Post;