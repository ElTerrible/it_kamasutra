import s from "./ProfileInfo.module.css";
import Preloader from "../../common/Preloader/Preloader";


const ProfileInfo = (props) =>{
    if (!props.profile){
        return <Preloader />
    }

    return (
        <div>
            <div>
                <img className={s.img} src="https://krasavica.info/uploads/posts/2019-12/thumbs/1576586336_breanne-benson-instagram-47.jpg" alt=""/>
            </div>
            <div className={s.descriptionBlock}>
                <div>
                    <img src={props.profile.photos.large}/>
                </div>
                <div>
                    {props.profile.aboutMe}
                </div>
                <div>
                    {props.profile.lookingForAJobDescription}
                </div>

                avatar + description
            </div>
        </div>
    )
}

export default ProfileInfo;