import s from './Header.module.css';
import {NavLink} from "react-router-dom";

const Header = (props) => {
    return <header className={s.header}>
                <img src="https://upload.wikimedia.org/wikipedia/commons/d/d5/Breanne_Benson_Exxxotica_Miami_2010.jpg" alt=""/>

                <div className={s.loginBlock}>
                    {
                        props.isAuth ? props.login : <NavLink to='/login'>Login</NavLink>
                    }

                </div>
           </header>
}


export default Header;